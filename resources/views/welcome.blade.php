<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>MARIE</title>
        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <style>
            html, body {
                height: 100%;
                background: rgba(10,10,10, 0.8);
                color:#F1F1F1;
            }

            a, a:visited, a:hover, a:link{
                color:#F1F1F1;
                text-decoration: none;
            }

            img{
                cursor:pointer;
            }

            p{
                vertical-align: middle;
            }

            .top{
                height:100px;
                position: fixed;
                top:0;
                width:100%;
                text-align: center;
            }

            .bottom{
                position: fixed;
                bottom:0;
                width:100%;
                padding-bottom:20px;
            }
            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
                width:90%;
                margin:auto;
                vertical-align: middle;
                padding:40px;
            }

            .title {
                font-size: 96px;
                vertical-align: middle;
            }

            .top .logo{
                margin-right:50px;
            }

            .title p{
                font-size: 14px;
            }

            .title .block{

            }

            .info-rec{
                position:absolute;
                left:0;
                top:0;
                padding:8px;
                margin-left:10px;
            }

            .info-hide{
                position:absolute;
                right:0;
                top:0;
                padding:8px;
                margin-right:10px;
                font-size:16px;
            }

            h1 {
                margin-top: 0;
                font-size:50px;
            }

            #msg {
                font-size: 0.9em;
                line-height: 1.4em;
            }

            #msg.not-supported strong {
                color: #CC0000;
            }

            input[type="text"] {
                width: 100%;
                padding: 0.5em;
                font-size: 1.2em;
                border-radius: 3px;
                border: 1px solid #D9D9D9;
                box-shadow: 0 2px 3px rgba(0,0,0,0.1) inset;
            }

            input[type="range"] {
                width: 300px;
            }

            label {
                display: inline-block;
                float: left;
                width: 150px;
            }

            .option {
                margin: 1em 0;
            }

            button {
                display: inline-block;
                border-radius: 3px;
                border: none;
                font-size: 0.9rem;
                padding: 0.5rem 0.8em;
                background: #69c773;
                border-bottom: 1px solid #498b50;
                color: white;
                -webkit-font-smoothing: antialiased;
                font-weight: bold;
                margin: 0;
                width: 100%;
                text-align: center;
            }

            button:hover, button:focus {
                opacity: 0.75;
                cursor: pointer;
            }

            button:active {
                opacity: 1;
                box-shadow: 0 -3px 10px rgba(0, 0, 0, 0.1) inset;
            }

            img{
                cursor:pointer;
            }

        </style>
    </head>
    <body>
        <div class="container">
            <div class="top">
                <span class="info-rec">Etat de la reconnaissance vocale : <img id="rec-state" src="img/rec-off.png" /></span>
                <img class="logo" src="{{ url('img/logo.png') }}" width="100" height="100">
                <a class="title">MARIE</a>
                <span class="info-hide active"><span class="badge">Cmd+Enter</span> pour Afficher/Masquer Marie !</span>
            </div>
            <div class="content">
                <h1 id="transcription">...</h1>
            </div>
            <div class="bottom">
                <img id="mic" class="not-animated" src="img/mic.png"/>
            </div>
        </div>
    </body>
    <script src="{{ url('js/jquery.js') }}" onload="window.$ = window.jQuery = module.exports;"></script>
    <script src="{{ url('js/ion.sound.min.js') }}"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <script>
        webview.addEventListener('permissionrequest', function(e) {
            if (e.permission === 'media') {
                e.request.allow();
            }
        });
    </script>
    <script src="{{ url('js/app.js') }}"></script>
</html>