<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modele extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'modeles';

    protected $fillable = [
        'modele', 'action', 'extra_params',
    ];
}
