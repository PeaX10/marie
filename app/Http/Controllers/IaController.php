<?php

namespace App\Http\Controllers;
use App\Modele;
use SpotifyWebAPI;

class IaController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @return Response
     */
    public function courtot($text){
        date_default_timezone_set('Europe/Paris');
        $text = strtolower(urldecode($text));
        switch($text){
            case 'bonjour':
                $reponse = 'Bonjour Alexandre';
                break;
            case 'quelle heure est-il':
                $reponse = 'Il est '.date('h:i');
                break;
            case 'appelle armel':
                $reponse = 'J\'appel Armel';
                break;
            case 'lecture de la musique':
                $reponse = 'Et que la musique soit !';
                exec('osascript -e \'tell app "Spotify" to play\'');
                break;
            case 'arrête la musique':
                $reponse = 'C\'est vrai quelle n\'était pas top !';
                exec('osascript -e \'tell app "Spotify" to pause\'');
                break;
            case 'musique suivante':
                $reponse = '';
                exec('osascript -e \'tell app "Spotify" to next track\'');
                break;
            case 'musique précédente':
                $reponse = '';
                exec('osascript -e \'tell app "Spotify" to previous track\'');
                exec('osascript -e \'tell app "Spotify" to previous track\'');
                break;
            case 'musique à zéro':
                $reponse = '';
                exec('osascript -e \'tell app "Spotify" to previous track\'');
                break;
            case 'musique à fond':
                $reponse = '';
                exec('osascript -e \'tell application "Spotify" set sound volume to 100\'');
                break;
            case 'lecture de reuf nekfeu':
                $reponse = 'Et c\'est parti !';
                exec('osascript -e \'tell application "Spotify" to play track "spotify:track:66Fyj5dF8mvXJKV1voPJF8"\'');
                break;
            case 'lecture de feu nekfeu':
                $reponse = 'Et c\'est parti !';
                exec('osascript -e \'tell application "Spotify" to play track "spotify:album:54fhnMUFTB9Y59JmgoCrM3"\'');
                break;
            case 'lecture de comment c\'est loin':
                $reponse = 'Et c\'est parti !';
                exec('osascript -e \'tell application "Spotify" to play track "spotify:album:1jfBSA8KgMLol7Iahnq8wv"\'');
                break;
            default:
                $reponse = 'Désolé mais je n\'ai pas compris';
                break;
        }
        echo json_encode($reponse);
    }

    public function understand($text){
        //On charge tout les modèles de la BDD
        $text = urldecode($text);
        $modeles = Modele::all();
        $reponse = array();
        // On compare notre phrase à tout les modèles jusqu'à ce que l'on en trouve un
        foreach($modeles as $modele){
            $data = $this->modele_texte($modele->modele, strtolower($text));
            if($data !== false){
              $action = $modele->action;
              $params = $modele->extra_params;
              break;
            }
        }
        // On vérfie que un modèle a bien été trouvé
        if($data !== false){
          // On convertis les actions en tableau
          $action = $this->actionToArray($action);
          $params = $this->paramsToArray($params, $data);
          $reponse = $this->buildActionReponse($action, $params);

        }else{
          // Aucun modèle n'a été trouvé
          $reponse = [['text' => 'Désolé mais je n\'ai pas compris']];
        }

        echo json_encode($reponse);
    }

    public function buildActionReponse($action, $params){
      $reponse = array();
      foreach($action as $key => $value){
        $push = true;
        switch($value['type']){
          case 'text':
            array_push($reponse, ['text' => $value['result']]);
            break;
          case 'spotify':
            // On définit les actions qui ont besoin d'être traité
            switch($value['result']){
              case 'track':
                  $api = new SpotifyWebAPI\SpotifyWebAPI();
                  $tracks = $api->search($params['track'], 'track')->tracks->items;
                  // On récupère uniquement le premier si il existe sinon il y a pas de résultats
                  if(isset($tracks[0])){
                    $track = $tracks[0];
                    $params['track'] = $track->id;
                  }else{
                    $push = false;
                    array_push($reponse, ['text' => 'Il n\'y a aucune chanson qui correspond à '. $params['track']]);
                  }
                break;
                case 'album':
                    $api = new SpotifyWebAPI\SpotifyWebAPI();
                    $albums = $api->search($params['album'], 'album')->albums->items;
                    // On récupère uniquement le premier si il existe sinon il y a pas de résultats
                    if(isset($albums[0])){
                      $album = $albums[0];
                      $params['album'] = $album->id;
                    }else{
                      $push = false;
                      array_push($reponse, ['text' => 'Il n\'y a aucun album qui correspond à '. $params['album']]);
                    }
                  break;
            }
            if($push) array_push($reponse, ['spotify' => ['action' => $value['result']], 'params' => $params]);
            break;
          case 'app':
            array_push($reponse, ['app' => ['action' => $value['result']], 'params' => $params]);
            break;
          case 'o-app':
            array_push($reponse, ['o-app' => ['action' => $value['result']], 'params' => $params]);
            break;
        }
      }

      return $reponse;
    }

    public function actionToArray($action){
      $action = explode('|', $action);
      foreach($action as $key => $value){
        $t = explode(':', $value);
        $reponse[$key] = ['type' => $t[0], 'result' => $t[1]];
      }
      return $reponse;
    }

    public function paramsToArray($params, $data){
      $reponse = array();
      $params = explode('|', $params);
      foreach($params as $key => $value){
        if($value != ''){
          $t = explode(':', $value);
          $reponse[$t[0]] = $data[$t[1]];
        }
      }
      return $reponse;
    }

    public function modele_texte($modele, $texte){

        // On transforme le modèle en tableau par rapport aux espaces et celui du texte également
        $t_modele = explode(' ', $modele);
        $t_texte = explode(' ', $texte);

        // On détermine à quel type modèle appartient
        /*
         * * : doute - obligatoirement précédé et suivi d'une clé ou précèdé d'une clé si dernière occurence
         * one_key : une seule clée - mot strict
         * (key1|key2|...) :  plusieurs clées - un de ses mots doit être entrée
         * (key1|key2|...|#variable) : plusieurs clés avec assimilation à une variable
         * #var : variable dans le mot - obligatoirement précédé et suivi d'une clé ou précèdé d'une clé si dernière occurence
         */

        foreach($t_modele as $key => $value){
            // Si elle commence par "(" alors c'est soit plusieurs clées soit plusieurs clées avec variable
            if($value{0} == '('){
                // On enleve les '(' ... ')' pour explode par |
                $multikey = substr($value,1,-1);
                $multikey = explode('|', $multikey);
                if($multikey[count($multikey) - 1]{0} == '#'){ // On teste pour savoir si la dernière occurrence commence par #
                    // Alors c'et un choix possible avec comme variable
                    $var = substr($multikey[count($multikey) - 1], 1); // Suppression de #
                    // On supprime la dernière valeur du tableau
                    unset($multikey[count($multikey)]);
                    $t_modele_type[$key] = ['type' => 'multikeys', 'keys' => $multikey, 'var' => $var];
                }else{
                    // C'est un simple multi clés
                    $t_modele_type[$key] = ['type' => 'multikeys', 'keys' => $multikey];
                }
            }
            // Si elle commence par '#' alors variable
            else if($value{0} == '#'){
                $var = substr($value, 1); // Suppression de #
                $t_modele_type[$key] = ['type' => 'var', 'var' => $var];
            }
            // Si c'est un doute
            else if($value == '*'){
                // Il peut y avoir n'importe quoi dedans
                $t_modele_type[$key] = ['type' => 'doute'];
            }
            // Sinon c'est un mot clé
            else{
                $t_modele_type[$key] = ['type' => 'key', 'key' => $value];
            }
        }
        // On vérifie que la structure de notre modele est correct
        if($this->verifModele($t_modele_type)){
        // Maintenant il nous faut associé notre phrase à chaque clé
        // On commence par associé les mots clés (multiple ou non dans notre phrase)
        $nb_modele = count($t_modele_type);
        $modele_validation = true;
        foreach($t_modele_type as $key => $value) {
            switch($value['type']){
                case 'key':
                    // On recherche la première occurrence du terme dans notre phrase
                    // Si il y a un tiret dans la clé, la phrase sera composée
                    if(strpos($value['key'], '_') !== false) {
                        // Là on décompose en un tableau avec la suite des clés
                        $keys = explode("_", $value['key']);
                        if(in_array($keys[0], $t_texte)){
                            // Alors la clé à été trouvé - Ajoute donc le texte correspondant
                            $key_temp = array_search($keys[0], $t_texte);
                            $text = $keys[0];
                            // On va donc rechercher si les termes suivant correspondent
                            $nb_mots = count($keys);
                            unset($keys[0]);
                            for($i = 1; $i <= $nb_mots - 1; $i++){
                                if($keys[$i] != $t_texte[$key_temp + $i]){
                                    $modele_validation = false;
                                }else{
                                    $text .= ' '.$keys[$i];
                                }
                            }
                        }else{
                            // Le mot n'existe pas
                            $modele_validation = false;
                        }
                    }else{
                        // Si le mot n'est pas composé on le recherche seulement
                        if(in_array($value['key'], $t_texte)){
                            $key_temp = array_search($value['key'], $t_texte);
                            $text = $value['key'];
                            $nb_mots = 1;
                        }else{
                            // Le mot n'existe pas
                            $modele_validation = false;
                        }
                    }
                    // Maintenant si tout c'est bien passé on update nos tableaux
                    if($modele_validation){
                        $t_modele_type[$key] = array('type' => '|', 'text' => $text);
                        // On supprime les clés que l'on ne veut plus dans $t_texte
                        for($i = 0; $i < $nb_mots; $i++){
                            unset($t_texte[$key_temp + $i]);
                        }
                    }
                    break;

                case 'multikeys':
                    // On recherche la première occurrence du terme dans notre phrase parmis les keys proposés
                    $multikeys = $value['keys'];
                    $found = false;
                    // Maintenant on va parcourrir tout notre tableau à la recherche du mot et à la fin on dit si il existe ou pas
                    foreach($multikeys as $onekey => $onevalue){
                        if(strpos($onevalue, '_') !== false) {
                            $modele_validation = true;
                            // Là on décompose en un tableau avec la suite des clés
                            $keys = explode("_", $onevalue);
                            if(in_array($keys[0], $t_texte)){
                                // Alors la clé à été trouvé - Ajoute donc le texte correspondant
                                $key_temp = array_search($keys[0], $t_texte);
                                $text = $keys[0];
                                // On va donc rechercher si les termes suivant correspondent
                                $nb_mots = count($keys) - 1;
                                unset($keys[0]);
                                for($i = 1; $i <= $nb_mots; $i++){
                                    if(array_key_exists($key_temp + $i, $t_texte) && $keys[$i] != $t_texte[$key_temp + $i]){
                                        $modele_validation = false;
                                    }else{
                                        $text .= ' '.$keys[$i];
                                    }
                                }
                                if($modele_validation){
                                    $found = true;
                                }
                            }
                        }else{
                            // Si le mot n'est pas composé on le recherche seulement
                            if(in_array($onevalue, $t_texte)){
                                $key_temp = array_search($onevalue, $t_texte);
                                $text = $onevalue;
                                $nb_mots = 1;
                                $found = true;
                            }
                        }
                    }
                    // Maintenant si tout c'est bien passé on update nos tableaux
                    if($found){
                        if(!empty($value['var'])){
                            $t_modele_type[$key] = array('type' => '|', 'text' => $text, 'var' => $value['var']);
                        }else{
                            $t_modele_type[$key] = array('type' => '|', 'text' => $text);
                        }
                        // On supprime les clés que l'on ne veut plus dans $t_texte
                        for($i = 0; $i < $nb_mots; $i++){
                            unset($t_texte[$key_temp + $i]);
                        }
                    }
                    break;
            }
        }
        // Si il reste des clés key ou multikeys alors le modèle est faux !
        foreach($t_modele_type as $key => $value){
          $notallow_type = ['key', 'multikeys'];
          if(in_array($value['type'], $notallow_type)){
            $modele_validation = false;
          }
        }
        // On s'arrête ici si le modèle ne correspond pas
        if(!$modele_validation){
          return false;
        }
        // Maintenant on va compléter les champs inconnu qu'il manque par rapport au texte
        $start = '';
        foreach($t_modele_type as $key => $value){
          // On va compléter start au fur et à mesure que l'on avance dans la boucle
          if($value['type'] == '|'){
            // Si c'est une valeur déjà déterminer
            $start .= $value['text'];
          }else{
            // Si c'est une valeur inconnu alors là on va récupèrer son texte entre le début et le mot d'après
            // Attention si c'est le dernier ou le premier mot
            if($key == count($t_modele_type)-1){
              $final_word = explode($start, $texte);
              $final_word = $final_word[1];
              $t_modele_type[$key]['text'] = substr($final_word, 1);
            }else if($key == 0){
              $final_word = explode($t_modele_type[$key + 1]['text'], $texte);
              $final_word = $final_word[0];
              $t_modele_type[$key]['text'] = substr($final_word, -1);
            }else{
              $final_word = explode($start, $texte);
              $final_word = $final_word[1];
              $final_word = explode($t_modele_type[$key + 1]['text'], $final_word);
              $final_word = $final_word[0];
              $t_modele_type[$key]['text'] = substr($final_word,1,-1);
            }
            $start .= $final_word;
          }
        }

        // Si aucune variable n'est vide on renvoi notre tableau sinon le modèle ne
        $vars = [];
        $modele_validation = true;
        foreach($t_modele_type as $key => $value){
          if(array_key_exists('var', $value)){
            if($value['text'] == false){
              $modele_validation = false;
            }else{
              $vars[$value['var']] = $value['text'];
            }
          }
        }
        if($modele_validation){
          //
          return $vars;
        }else{
          return false;
        }

        }
    }

    public function verifModele($modele){
        // On commence par définir les types de caractères ne pouvant pas se suivre
        $cara = ['var', 'doute'];
        $modele_verif = true;
        // On parcours toute la liste des type de modeles
        foreach($modele as $key => $value){
            // Si un type n'est pas autorisé
            if(in_array($value['type'], $cara)){
                // Si il est premier ou dernier
                if($key == 0){
                    // Si il est premier on vérifie que la clé suivante n'appartient au type qui ne doivent pas se suivre
                    // On récupère simplement ce qu'il y a avant le deuxieme type
                    if(in_array($modele[$key + 1]['type'], $cara)) $modele_verif = false;
                }else if($key == count($modele) - 1){
                    // Si il est dernier
                    if(in_array($modele[$key - 1]['type'], $cara)) $modele_verif = false;
                }else{
                    // On vérifie que celui avant et celui après appartienent pas aux types qui ne doivent pas se suivre
                    if(in_array($modele[$key - 1]['type'], $cara) || in_array($modele[$key + 1]['type'], $cara)) $modele_verif = false;
                }
            }
        }

        return $modele_verif;
    }

    public function getAction($action){
        return false;
    }

}
