// Reconnaissance vocale
var recognizer = null;
var marieDetected = false;
var transcription = document.getElementById('transcription');
// Synthese vocale
var voices = speechSynthesis.getVoices();
var msg = new SpeechSynthesisUtterance();
var ipc = require('electron').ipcRenderer;

function speak(text) {

    msg.text = text;

    msg.volume = 1.;
    msg.rate = 1.;
    msg.pitch = 1.;

    msg.voice = speechSynthesis.getVoices().filter(function(voice) { return voice.name == 'Aurelie'; })[0];
    msg.lang = 'fr-FR';

    msg.onstart = function(event){

    };
    msg.onend = function(event){
        recognizer.start();
    };

    speechSynthesis.speak(msg);
    console.log(msg);
}

function recognize()
{
    // Récupération du moteur de reconnaissance vocale
    window.speechRecognition = window.speechRecognition || window.webkitSpeechRecognition || window.mozSpeechRecognition || window.webkitSpeechRecognition;

    if(window.speechRecognition == undefined)
    {
        alert("La reconnaissance vocale n'est pas supporté");
    }
    else
    {
        recognizer = new speechRecognition();

        // Recognizer Options
        recognizer.continuous = true;
        recognizer.lang = "fr-FR";
        recognizer.interimResults = true;

        // Reconnaissance vocale activé
        recognizer.onstart = function(){
            recOn();
        }

        // Quand la reconnaissance de Marie est coupé
        recognizer.onend = function(){
            recOff();
            if(marieDetected == false){
                recognizer.continuous = true;
                recognizer.interimResults = true;
                // On veut savoir si il va parler
                if(speechSynthesis.pending || speechSynthesis.speaking){
                    // On fait rien
                    recognizer.stop();
                }else{
                    recognizer.start();
                }

            }else{
                ipc.send('MarieOn', true);
                marieDetected = false;
                recognizer.continuous = false;
                recognizer.interimResults = false;
                recognizer.start();
                StartMic();
            }
        }

        // Analyse des résultats
        recognizer.onresult = function(event){
            //event.resultIndex returns the index of first word spoken in the currently stoped sentence.
            //event.results.length is the total number of words spoken in this session.
            for(var count = event.resultIndex; count < event.results.length; count++){
                if(Marie(event.results[count][0].transcript)){
                    marieDetected = true;
                    recognizer.stop();
                }else if(recognizer.interimResults == false){
                    transcription.textContent = event.results[count][0].transcript;
                    $.ajax({
                        method: "GET",
                        url: "http://marie.dev/ia/" + encodeURIComponent(event.results[count][0].transcript)
                    }).done(function( msg ) {
                        recognizer.stop();
                        msg = jQuery.parseJSON(msg);
                        transcription.textContent = msg;
                        speak(msg);
                        StopMic();
                        ipc.send('MarieOff', true);
                    });
                }else{
                    console.log(event.results[count][0].transcript);
                }
            }
        }

        recognizer.onerror = function(event){
            //alert('Speech recognition error detected: ' + event.error);
            //alert('Additional information: ' + event.message);
        }

        recognizer.start();
    }
}

// Sons
ion.sound({
    sounds: [
        {
            name: "start"
        },
        {
            name: "end"
        }
    ],
    volume: 1,
    path: "sound/",
    preload: true
});

function stopRecognize(){
    recognizer.stop();
}

function Marie(str){
    str = str.toLowerCase();
    if((str.indexOf("marie") >= 0) || (str.indexOf("mari") >= 0)|| (str.indexOf("mary") >= 0)){
        return true;
    }else{
        return false;
    }
}

function StartMic(){
    $('#mic').attr('src', './img/mic.gif');
    $('#mic').addClass('animated');
    $('#mic').removeClass('not-animated');
    $('#transcription').text('...');
    ion.sound.play("start");
}

function StopMic(queue){
    $('#mic').attr('src', './img/mic.png');
    $('#mic').addClass('not-animated');
    $('#mic').removeClass('animated');
    if(queue) window.speechSynthesis.cancel();
    ion.sound.play("end");
}

function recOn(){
    $('#rec-state').attr('src', './img/rec-on.png');
}

function recOff(){
    $('#rec-state').attr('src', './img/rec-off.png');
}

$('#mic').click(function(){
    if($('#mic').hasClass('not-animated')){
        marieDetected = true;
        recognizer.stop();
        StartMic();
    }else{
        stopRecognize();
        StopMic();
    }
});

speak('Bienvenue...');

recognize();